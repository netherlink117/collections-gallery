# Collections Gallery
Collections Gallery, as it's name describes, is a web gallery... It's another more...
## Project setup
Just run the install script...
```
yarn install
```
### Compiles and hot-reloads for development
Use this command to serve and debug...
```
yarn serve
```
### Compiles and minifies for production
Use this command to build (compile) and get a dist (public) folder...
```
yarn build
```
### Lints and fixes files
Use this command if you only whant to run lint to get warnings on your code or format it...
```
yarn lint
```
### Customize configuration
This proyect is writen with Vue.js, furter information is available at it's official site. See [Configuration Reference](https://cli.vuejs.org/config/).

## License
The code shared on this repository is shared under [GNU AGPLv3](https://opensource.org/licenses/AGPL-3.0) license.